# Julian Martin :: Resume

📧 ar.julian.martin@gmail.com  -  🌐 www.linkedin.com/in/ar-julian-martin  -  📞 +541168871820

> I enjoy turning complex things into simple ones and I think software is a great tool to do it.
Currently looking for challanging oportunities.

## 🛠 Skills

### Javascript / Typescript ⭐️⭐️⭐️⭐️

6+ years experience from **HTML5+ vanilla** **Javascript** to **Angular** & **React** with **Typescript**.

**RXJS - Redux - Jest - Cypress - Express - MongoDB**

### DevOps ⭐️⭐️⭐️

Migrated entire software process **DevOps** on **Azure**
raising code quality with continuous improvement 

**CI/CD - Kubernetes - GIT - Docker - AWS**

### Python ⭐️⭐️⭐️

**Django** for backend and **Flask** for small API’s.
Simple data science projects & scripting

### Web3 ⭐️⭐️

Basic integrations with ETH Blockchain with **web3.js** .

Training projects on **Solidity** with **Truffle** + **Ganache** 
> I would love to gain some experience contributing from my frontend background.


## 💻 Work experience

### Full Stack Dev - Solution Architect
***NCR**, Argentina – (2018 - Today)*

- Built modern webapps based on **Angular**, **Rxjs** and **React.**
- Created own libraries from scratch.
- Implemented **Jest** & **cypress** to automate testing. 
- Migrated entire software process to **DevOps** on **Azure** mixed with **Scrum** and **CI/CD.**
- Legacy projects based on **nodejs** own framework, **AngularJS** portal and **Django** server.
- Helped on server migration to Springboot.

### Freelance Developer
***Freelance**, Argentina — (2017-2018)*

- Webapp based on **Angular 4**
- Cleaned old **MySQL** database and build a **REST API** on top of it with **express**

### Tutor at Tutor.com
***Freelance**, Online — (2018)*

- Science and Math’s Tutor in English


## 📚 Education

### **Software Engineering**
***UBA**, Buenos Aires (2014 - 2019 unfinished)*

## 🗣 Languages

- Native Spanish 🇪🇸 🇦🇷
- Fluent English 🇺🇸
- Basic Italian 🇮🇹